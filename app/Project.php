<?php

namespace HakaAdmin;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['photo','name', 'client_id', 'startDate', 'endDate','priority', 'leader_id', 'description'];

    public function tasks(){
        return $this->hasMany(Task::class);
    }

    public function pendingTasks(){
        return $this->hasMany(Task::class)->where('completed',0);
    }

    public function completeTasks(){
        return $this->hasMany(Task::class)->where('completed',1);
    }

    public function progress(){
        
        $completeTasks = $this->hasMany(Task::class)->where('completed',1)->get()->count();
        $totalTasks = $this->hasMany(Task::class)->get()->count();

        $progressProject = round(($completeTasks/$totalTasks) * 100);
        
        return $progressProject;
    }

    public function lider(){
    	return $this->belongsTo(User::class, 'leader_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}

