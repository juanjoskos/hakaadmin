<?php

namespace HakaAdmin;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'user_id',
        'description'
    ];

    public function user(){
    	return $this->belongsTo(User::class);
    }
}
