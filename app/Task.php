<?php

namespace HakaAdmin;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'name',
        'user_id',
        'project_id',
        'startDate',
        'endDate',
        'priority',
        'description'
    ];

    public function project(){
    	return $this->belongsTo(Project::class);
    }
    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function comments(){
    	return $this->belongsToMany(Comment::class);
    }
}
