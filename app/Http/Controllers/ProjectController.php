<?php

namespace HakaAdmin\Http\Controllers;
use Illuminate\Http\Request;

use HakaAdmin\Project;
use HakaAdmin\Client;
use HakaAdmin\User;
use HakaAdmin\Task;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $clients = Client::all();
        $users = User::all();
        $projects = Project::all();


        return view('projects.projects', compact('projects','clients','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nameFile = "";

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $nameFile = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/projects/',$nameFile);
        }

        $project = new Project($request->except('team','photo'));
        $project->photo = $nameFile;
        $newSlug = str_replace(" ", "-", $request->name);
        $project->slug = strtolower($newSlug)."-".$project->id;
        $project->save();

        foreach ($request->team as $key => $value) {
            $project->users()->attach($value);
        }

        return  redirect()->route('projects.index')->with('status', 'Proyecto registrado exitosamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \HakaAdmin\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $project = Project::where('slug', $slug)->firstOrFail();
        $tasks = $project->tasks;
        $completeTasks = Task::where('completed', '1')->get()->count();
        $incompleteTasks = Task::where('completed', '1')->get()->count();

        if($tasks->count()  > 0)
            $progressProject = round(($completeTasks/$tasks->count()) * 100);
        else
            $progressProject = 0;

        $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i',  $project->startDate.' 3:30:34');
        $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $project->endDate.' 9:30:34');
        $today = \Carbon\Carbon::now();

        $totalHours = $to->diffInHours($from);
        $leftHours = $today->diffInHours($from);

        return view('projects.show', compact(
            'project',
            'completeTasks',
            'incompleteTasks',
            'tasks',
            'progressProject',
            'totalHours',
            'leftHours'
        ));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \HakaAdmin\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \HakaAdmin\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \HakaAdmin\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }
}
