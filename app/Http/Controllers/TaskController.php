<?php

namespace HakaAdmin\Http\Controllers;

use HakaAdmin\Task;
use HakaAdmin\Project;
use HakaAdmin\Client;
use HakaAdmin\User;
use HakaAdmin\Comment;


use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $isTask = true;
        $projects = Project::all();
        $clients = Client::all();
        $users = User::all();
        $tasks = Task::all();

        return view('tasks.indexTask', compact('projects','clients','users','isTask','tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newTask = new Task($request->all());
        $newTask->save();

        if($request->ajax()){
            return response()->json([
                'status' => true,
                'task' => $newTask,
                'user' => $newTask->user
            ]);
        }
    }

    public function GetTask(Request $request){
        $task = Task::where('id',$request->task_id)->with('user','comments','comments.user')->first();
        if($request->ajax()){
            return response()->json([
                'status' => true,
                'task' => $task,
            ]);
        }
    }

    public function DeleteComment(Request $request){
        $comment = Comment::find($request->comment_id);
        $comment->delete();

        if($request->ajax()){
            return response()->json([
                'status' => true
            ]);
        }
    }

    public function UpdateTask(Request $request){
        $task = Task::find($request->task_id);
        $task->name = $request->name;
        $task->porcentage = $request->porcentage;
        $task->completed = $request->completed;

        $task->save();

        if($request->ajax()){
            return response()->json([
                'status' => true
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \HakaAdmin\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    public function AddCommentTask(Request $request)
    {
        $task = Task::find($request->task_id);
        $comment = Comment::create($request->except('task_id'));
        $task->comments()->attach($comment->id);

        if($request->ajax()){
            return response()->json([
                'status' => true,
                'comment' => $comment,
                'user' => $comment->user
            ]);
        }
    }

    

    public function ProjectTask($slug)
    {
        $project = Project::where('slug', $slug)->firstOrFail();
        $isTask = true;
        $projects = Project::all();
        $clients = Client::all();
        $users = User::all();
        $tasks = $project->tasks;

        return view('tasks.task', compact('projects','clients','users','isTask','tasks','project'));
    }

    public function DeleteTask(Request $request)
    {
        $task = Task::find($request->task_id);
        $task->delete();

        if($request->ajax()){
            return response()->json([
                'status' => true
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \HakaAdmin\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \HakaAdmin\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \HakaAdmin\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
}
