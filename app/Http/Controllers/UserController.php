<?php

namespace HakaAdmin\Http\Controllers;

use Illuminate\Http\Request;

use HakaAdmin\User;

class UserController extends Controller
{
    public function GetInfo(Request $request){
        $users = User::find($request->team);

        return response([
            'status' => true,
            'users' => $users
        ]);
    }
}
