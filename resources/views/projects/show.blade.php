@extends('layouts.admin')

@section('title', 'Proyectos')

@section('css')
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

@endsection
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
        <div class="row">
            {{--  <div class="col-xs-8">
                <h4 class="page-title">{{ $project->name }}</h4>
            </div>  --}}
            <div class="col-sm-4 text-right m-b-30 pull-right">
                <a href="#" class="btn btn-primary rounded" data-toggle="modal" data-target="#edit_project"><i class="fa fa-plus"></i> Edit Project</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9">
                <div class="panel">
                    <div class="panel-body">
                        <div class="project-title">
                            <h5 class="panel-title">{{ $project->name }}</h5>
                            <img src="{{ asset( 'images/projects/'.$project->photo ) }}" height="192" width="192" center="">
                           
                            <small class="block text-ellipsis m-b-15"><span class="text-xs">{{ count($project->pendingTasks) }}</span> <span class="text-muted">Tareas Incompletas, </span><span class="text-xs">{{ count($project->completeTasks) }}</span> <span class="text-muted">Tareas Completas</span></small>
                        </div>
                        <p>{!! $project->description !!}</p>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <h5 class="panel-title m-b-20">Observaciones</h5>
                        {!! $project->observations !!}
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <h5 class="panel-title m-b-40">Imagenes Subidas</h5>
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <div class="thumbnail">
                                    <div class="thumb">
                                        <img src="{{asset('images/logo2.jpg')}}" class="img-responsive" alt="">
                                    </div>
                                    <div class="caption text-center">
                                            demo.png
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="thumbnail">
                                    <div class="thumb">
                                        <img src="{{asset('images/logo2.jpg')}}" class="img-responsive" alt="">
                                    </div>
                                    <div class="caption text-center">
                                            demo.png
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="thumbnail">
                                    <div class="thumb">
                                        <img src="{{asset('images/logo2.jpg')}}" class="img-responsive" alt="">
                                    </div>
                                    <div class="caption text-center">
                                            demo.png
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="thumbnail">
                                    <div class="thumb">
                                        <img src="{{asset('images/logo2.jpg')}}" class="img-responsive" alt="">
                                    </div>
                                    <div class="caption text-center">
                                            demo.png
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        <h5 class="panel-title m-b-20">Archivos Subidos</h5>
                        <ul class="files-list">
                            <li>
                                <div class="files-cont">
                                    <div class="file-type">
                                        <span class="files-icon"><i class="fa fa-file-pdf-o"></i></span>
                                    </div>
                                    <div class="files-info">
                                        <span class="file-name text-ellipsis"><a href="#">AHA Selfcare Mobile Application Test-Cases.xls</a></span>
                                        <span class="file-author"><a href="#">John Doe</a></span> <span class="file-date">May 31st at 6:53 PM</span>
                                        <div class="file-size">Size: 14.8Mb</div>
                                    </div>
                                    <ul class="files-action">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle btn btn-default btn-xs" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0)">Download</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#share_files">Share</a></li>
                                                <li><a href="javascript:void(0)">Delete</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="files-cont">
                                    <div class="file-type">
                                        <span class="files-icon"><i class="fa fa-file-pdf-o"></i></span>
                                    </div>
                                    <div class="files-info">
                                        <span class="file-name text-ellipsis"><a href="#">AHA Selfcare Mobile Application Test-Cases.xls</a></span>
                                        <span class="file-author"><a href="#">Richard Miles</a></span> <span class="file-date">May 31st at 6:53 PM</span>
                                        <div class="file-size">Size: 14.8Mb</div>
                                    </div>
                                    <ul class="files-action">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle btn btn-default btn-xs" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0)">Download</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#share_files">Share</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="project-task">
                    <div class="tabbable">
                        <ul class="nav nav-tabs nav-tabs-top nav-justified m-b-0">
                            <li class="active"><a href="#all_tasks" data-toggle="tab" aria-expanded="true">Totas las Tareas</a></li>
                            <li><a href="#pending_tasks" data-toggle="tab" aria-expanded="false">Tareas Pendientes</a></li>
                            <li><a href="#completed_tasks" data-toggle="tab" aria-expanded="false">Tareas Completadas</a></li>
                        </ul>
                        <div class="tab-content" style="height: 412px; overflow-y: auto; margin-top: 0 !important;">
                            <div class="tab-pane active" id="all_tasks">
                                <div class="table-responsive manage-table task-wrapper">
                                    <table class="table " cellspacing="14" id="task-list">
                                        <thead>
                                            <tr>
                                                {{--  <th></th>
                                                <th></th>
                                                <th width="650">NOMBRE</th>
                                                <th>PROGRESO</th>
                                                <th>ESTADO</th>
                                                <th>PRIORIDAD</th>
                                                <th>USUARIO</th>
                                                <th>ACCIONES</th>  --}}
                                            </tr>
                                        </thead>
                                        <tbody  >
                                            @foreach ($tasks as $task)
                                                <tr  class="advance-table-row task-container task {!! $task->completed == '1'? 'completed active': '' !!}" id="{{ $task->id }}">
                                                    <td width="10"></td>
                                                    <td width="40">
                                                        <span id="{{$task->completed}}" class="task-action-btn task-check">
                                                            <span class="action-circle large complete-btn" title="Marcar Completa">
                                                                <i class="material-icons">check</i>
                                                            </span>
                                                        </span>
                                                    </td>
                                                    <td width="720">
                                                        <span class="task-label" contenteditable="false">{{ $task->name }}</span>
                                                    </td>
                                                    <td width="320">
                                                        <div class="bar-container">
                                                            <div class="progress">
                                                                @if (empty($task->porcentage))
                                                                    <div class="progress-bar"></div>
                                                                @else
                                                                    <div id="{{$task->porcentage}}" class="progress-bar  progress-{{$task->porcentage}}"></div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="btn-group dropdown m-r-10">
                                                            <button aria-expanded="false" data-toggle="dropdown" class="rounded btn btn-{!! str_replace(' ','-',$task->status) !!} dropdown-toggle waves-effect waves-light" type="button">
                                                                {{ $task->status }}
                                                            </button>
                                                          
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="btn-group m-r-10">
                                                            <button aria-expanded="false" data-toggle="dropdown" class="rounded btn btn-info dropdown-toggle waves-effect waves-light" type="button">Normal</button>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        @php
                                                            setlocale(LC_ALL,"es_ES");
                                                            $date = date_create($task->endDate)
                                                        @endphp
                                                        {!! date_format($date, 'M jS') !!}
                                                    </td>
                                                    <td width="60" ><img title="{{ $task->user->name }}" src="../plugins/images/users/varun.jpg" class="img-circle" width="30" /></td>
                                                </tr>
                                                <tr id="separator-{{ $task->id }}">
                                                    <td colspan="7" class="sm-pd"></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="pending_tasks">
                                <div class="table-responsive manage-table task-wrapper">
                                    <table class="table " cellspacing="14" id="task-list">
                                        <thead>
                                            <tr>
                                                {{--  <th></th>
                                                <th></th>
                                                <th width="650">NOMBRE</th>
                                                <th>PROGRESO</th>
                                                <th>ESTADO</th>
                                                <th>PRIORIDAD</th>
                                                <th>USUARIO</th>
                                                <th>ACCIONES</th>  --}}
                                            </tr>
                                        </thead>
                                        <tbody  >
                                            @foreach ($tasks as $task)
                                                @if ($task->completed == '0')   
                                                <tr  class="advance-table-row task-container task {!! $task->completed == '1'? 'completed active': '' !!}" id="{{ $task->id }}">
                                                        <td width="10"></td>
                                                        <td width="40">
                                                            <span id="{{$task->completed}}" class="task-action-btn task-check">
                                                                <span class="action-circle large complete-btn" title="Marcar Completa">
                                                                    <i class="material-icons">check</i>
                                                                </span>
                                                            </span>
                                                        </td>
                                                        <td width="720">
                                                            <span class="task-label" contenteditable="false">{{ $task->name }}</span>
                                                        </td>
                                                        <td width="320">
                                                            <div class="bar-container">
                                                                <div class="progress">
                                                                    @if (empty($task->porcentage))
                                                                        <div class="progress-bar"></div>
                                                                    @else
                                                                        <div id="{{$task->porcentage}}" class="progress-bar  progress-{{$task->porcentage}}"></div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="btn-group dropdown m-r-10">
                                                                <button aria-expanded="false" data-toggle="dropdown" class="rounded btn btn-{!! str_replace(' ','-',$task->status) !!} dropdown-toggle waves-effect waves-light" type="button">
                                                                    {{ $task->status }}
                                                                </button>
                                                              
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="btn-group m-r-10">
                                                                <button aria-expanded="false" data-toggle="dropdown" class="rounded btn btn-info dropdown-toggle waves-effect waves-light" type="button">Normal</button>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            @php
                                                                setlocale(LC_ALL,"es_ES");
                                                                $date = date_create($task->endDate)
                                                            @endphp
                                                            {!! date_format($date, 'M jS') !!}
                                                        </td>
                                                        <td width="60" ><img title="{{ $task->user->name }}" src="../plugins/images/users/varun.jpg" class="img-circle" width="30" /></td>
                                                    </tr>
                                                    <tr id="separator-{{ $task->id }}">
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="completed_tasks">
                                <div class="table-responsive manage-table task-wrapper">
                                    <table class="table " cellspacing="14" id="task-list">
                                        <thead>
                                            <tr>
                                                {{--  <th></th>
                                                <th></th>
                                                <th width="650">NOMBRE</th>
                                                <th>PROGRESO</th>
                                                <th>ESTADO</th>
                                                <th>PRIORIDAD</th>
                                                <th>USUARIO</th>
                                                <th>ACCIONES</th>  --}}
                                            </tr>
                                        </thead>
                                        <tbody  >
                                            @foreach ($tasks as $task)
                                                @if ($task->completed == '1')   
                                                <tr  class="advance-table-row task-container task {!! $task->completed == '1'? 'completed active': '' !!}" id="{{ $task->id }}">
                                                        <td width="10"></td>
                                                        <td width="40">
                                                            <span id="{{$task->completed}}" class="task-action-btn task-check">
                                                                <span class="action-circle large complete-btn" title="Marcar Completa">
                                                                    <i class="material-icons">check</i>
                                                                </span>
                                                            </span>
                                                        </td>
                                                        <td width="720">
                                                            <span class="task-label" contenteditable="false">{{ $task->name }}</span>
                                                        </td>
                                                        <td width="320">
                                                            <div class="bar-container">
                                                                <div class="progress">
                                                                    @if (empty($task->porcentage))
                                                                        <div class="progress-bar"></div>
                                                                    @else
                                                                        <div id="{{$task->porcentage}}" class="progress-bar  progress-{{$task->porcentage}}"></div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="btn-group dropdown m-r-10">
                                                                <button aria-expanded="false" data-toggle="dropdown" class="rounded btn btn-{!! str_replace(' ','-',$task->status) !!} dropdown-toggle waves-effect waves-light" type="button">
                                                                    {{ $task->status }}
                                                                </button>
                                                              
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="btn-group m-r-10">
                                                                <button aria-expanded="false" data-toggle="dropdown" class="rounded btn btn-info dropdown-toggle waves-effect waves-light" type="button">Normal</button>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            @php
                                                                setlocale(LC_ALL,"es_ES");
                                                                $date = date_create($task->endDate)
                                                            @endphp
                                                            {!! date_format($date, 'M jS') !!}
                                                        </td>
                                                        <td width="60" ><img title="{{ $task->user->name }}" src="../plugins/images/users/varun.jpg" class="img-circle" width="30" /></td>
                                                    </tr>
                                                    <tr id="separator-{{ $task->id }}">
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel">
                    <div class="panel-body">
                        <h6 class="panel-title m-b-15">Detalles del Proyecto</h6>
                        <table class="table table-striped table-border">
                            <tbody>
                                <tr>
                                    <td>Costo:</td>
                                    <td class="text-right">$0 MXN</td>
                                </tr>
                                <tr>
                                    <td>Total Horas:</td>
                                    <td class="text-right">{{$totalHours}} Hrs</td>
                                </tr>
                                <tr>
                                    <td>Horas Restantes:</td>
                                    <td class="text-right">{{$leftHours}} Hrs</td>
                                </tr>
                                <tr>
                                    <td>Inicio:</td>
                                    @php
                                        setlocale(LC_ALL,"es_ES");
                                        $date = date_create($project->startDate)
                                    @endphp
                                    <td class="text-right"> {!! date_format($date, 'j M Y') !!}</td>
                                </tr>
                                <tr>
                                    <td>Entrega:</td>
                                    @php
                                        setlocale(LC_ALL,"es_ES");
                                        $date = date_create($project->endDate)
                                    @endphp
                                    <td class="text-right"> {!! date_format($date, 'j M Y') !!}</td>
                                </tr>
                                <tr>
                                    <td>Prioridad:</td>
                                    <td class="text-right">
                                        <div class="btn-group">
                                            <a href="#" class="label label-danger dropdown-toggle" data-toggle="dropdown">{{ $project->priority }}<span class="caret"></span></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="fa fa-dot-circle-o text-danger"></i> Más alta</a></li>
                                                <li><a href="#"><i class="fa fa-dot-circle-o text-info"></i> Alta</a></li>
                                                <li><a href="#"><i class="fa fa-dot-circle-o text-primary"></i> Normal</a></li>
                                                <li><a href="#"><i class="fa fa-dot-circle-o text-success"></i> Baja</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Estado:</td>
                                    <td class="text-right">
                                        <div class="btn-group">
                                            <a href="#" class="label label-danger dropdown-toggle" data-toggle="dropdown">Trabajando<span class="caret"></span></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="fa fa-dot-circle-o text-danger"></i> Terminado</a></li>
                                                <li><a href="#"><i class="fa fa-dot-circle-o text-info"></i> Pausa</a></li>
                                                <li><a href="#"><i class="fa fa-dot-circle-o text-primary"></i> Cancelado</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p class="m-b-5">Progreso <span class="text-success pull-right">{{count($project->tasks) > 0 ? $project->progress() : 0}}%</span></p>
                        <div class="progress progress-xs m-b-0">
                            <div class="progress-bar progress-bar-success" role="progressbar" data-toggle="tooltip" title="{{count($project->tasks) > 0 ? $project->progress() : 0}}%" style="width: {{count($project->tasks) > 0 ? $project->progress() : 0}}%"></div>
                        </div>
                    </div>
                </div>
                <div class="panel project-user">
                    <div class="panel-body">
                        <h6 class="panel-title m-b-20">Lider Asignado
                            <a class="pull-right btn btn-primary btn-xs" data-toggle="modal" data-target="#assign_leader"><i class="fa fa-plus"></i> Add</a></h6>
                        <ul class="list-box">
                            <li>
                                <a href="profile.html">
                                    <div class="list-item">
                                        <div class="list-left">
                                            <img class="avatar" src="{{asset('/images/users/'.$project->lider->photo)}}" alt="{{ $project->lider->username }}">
                                        </div>
                                        <div class="list-body">
                                            <span class="message-author">{{ $project->lider->name }}</span>
                                            <div class="clearfix"></div>
                                            <span class="message-content">{{ $project->lider->role }}</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="panel project-user">
                    <div class="panel-heading">
                        <h6 class="panel-title">Usuarios Asignados <a class="pull-right btn btn-primary btn-xs" data-toggle="modal" data-target="#assign_user"><i class="fa fa-plus"></i> Add</a></h6>
                    </div>
                    <div class="panel-body">
                        <ul class="list-box">
                            @foreach ($project->users as $user)
                                <li>
                                    <a href="profile.html">
                                        <div class="list-item">
                                            <div class="list-left">
                                                <img class="avatar" src="{{asset('/images/users/'.$user->photo)}}" alt="{{ $user->username }}">
                                            </div>
                                            <div class="list-body">
                                                <span class="message-author">{{$user->name}}</span>
                                                <div class="clearfix"></div>
                                                <span class="message-content">{{$user->role}}</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>  
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection

