<div id="create_project" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">Crear Proyecto</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('projects.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="white-box">
                                <label>Foto del Proyecto</label>
                                <input name="photo" type="file" id="input-file-now-custom-1" class="dropify" data-default-file="{{ asset('images/logo2.jpg') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="white-box">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="inputGroup inputGroup1">
                                    <label>Nombre</label>
                                    <input type="text" id="email" class="input-form" name="name" required>
                                    <p class="helper helper1">Nombre del proyecto</p>
                                    <span class="indicator"></span>
                                </div>    
                            </div>
                            <div class="col-md-6">
                                <div class="inputGroup inputGroup1">
                                    <label>Cliente</label>
                                    <select name="client" class="select input-form">
                                        <option value=""></option>
                                        @foreach ($clients as $client)
                                            <option value="{{ $client->id }}">{{$client->name}}</option>
                                        @endforeach
                                      
                                    </select>
                                    <p class="helper helper1">Cliente</p>
                                    <span class="indicator"></span>
                                </div>   
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="inputGroup inputGroup1">
                                    <label>Inicio</label>
                                    <input name="startDate" class="input-form datetimepicker" type="text" required>
                                    <p class="helper helper1">Fecha de Inicio</p>
                                    <span class="indicator"></span>
                                </div>    
                            </div>
                            <div class="col-md-6">
                                <div class="inputGroup inputGroup1">
                                    <label>Final</label>
                                    <input name="endDate" class="input-form datetimepicker" type="text" required>
                                    <p class="helper helper1">Fecha de Entrega</p>
                                    <span class="indicator"></span>
                                </div>    
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="inputGroup inputGroup1">
                                    <label>Prioridad</label>
                                    <select name="priority" class="select input-form" required>
                                        <option></option>
                                        <option>Baja</option>
                                        <option>Media</option>
                                        <option>Alta</option>
                                    </select>
                                    <p class="helper helper1">Prioridad</p>
                                    <span class="indicator"></span>
                                </div>
                            </div>   
                            <div class="col-md-6">
                                <div class="inputGroup inputGroup1">
                                    <label>Lider</label>
                                    <select name="leader_id" class="select input-form" required>
                                        <option></option>
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{$user->username}}</option>
                                        @endforeach
                                    </select>
                                    <p class="helper helper1">Lider del Proyecto</p>
                                    <span class="indicator"></span>
                                </div>   
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="inputGroup inputGroup1">
                                    <label>Equipo</label>
                                    <select id="team" class="js-example-basic-multiple select input-form" name="team[]" multiple="multiple" required>
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{$user->username}}</option>
                                        @endforeach
                                    </select>
                                </div>   
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Miembros del Equipo </label>
                                    <div id="teamContent" class="project-members">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="inputGroup inputGroup1">
                                <label>Descripción</label>
                                <textarea style="padding: 22px 1em 0px;" name="description" rows="4" cols="5" class="input-form summernote"></textarea>
                                <span class="indicator"></span>
                            </div>   
                        </div>
                    </div>
                    <div class="white-box">
                        <div class="m-t-20 text-center">
                            <button class="button-haka">Crear Proyecto</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

