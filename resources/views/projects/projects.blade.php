@extends('layouts.admin')

@section('title', 'Proyectos')

@section('css')
    <link href="{{ asset('plugins/bower_components/register-steps/steps.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
    <link href="../plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <link href="../plugins/bower_components/jquery-asColorPicker-master/dist/css/asColorPicker.css" rel="stylesheet">
    <link href="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="../plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="plugins/summernote/dist/summernote.css" rel="stylesheet">
    <link href="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="../plugins/bower_components/custom-select/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="../plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="../plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="../plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="../plugins/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />

@endsection

@section('content')

<div class="row">
    <div class="col-xs-4">
        <h4 class="page-title">Proyectos</h4>
    </div>
    <div class="col-xs-8 text-right m-b-30">
        <a href="#" class="btn btn-primary rounded pull-right" data-toggle="modal" data-target="#create_project"><i class="fa fa-plus"></i> Crear Proyecto</a>
        <div class="view-icons">
            <a href="{{ route('projects.index') }}" class="grid-view btn btn-link active"><i class="fa fa-th"></i></a>
            <a href="#" class="list-view btn btn-link"><i class="fa fa-bars"></i></a>
        </div>
    </div>
</div>
@include('commons.alert')
@foreach ($projects as $project)
    <div class="col-lg-3 col-sm-4">
        <div class="card-box project-box">
            <div class="dropdown profile-action">
                <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <ul class="dropdown-menu pull-right">
                    <li><a href="/task-project/{{$project->slug}}"><i class="fa fa-pencil m-r-5"></i> Tareas</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#edit_project"><i class="fa fa-pencil m-r-5"></i> Editar</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#delete_project"><i class="fa fa-trash-o m-r-5"></i> Eliminar</a></li>
                </ul>
            </div>
            <h4 class="project-title"><a href="/projects/{{ $project->slug }}">{{ $project->name }}</a></h4>
            <img src="{{ asset( 'images/projects/'.$project->photo ) }}" height="192" width="192" center="">
            <small class="block text-ellipsis m-b-15">
                <span class="text-xs">{{ count($project->pendingTasks) }}</span> <span class="text-muted">Tareas Abiertas, </span>
                <span class="text-xs">{{ count($project->completeTasks) }}</span> <span class="text-muted">Tareas Completadas</span>
            </small>
            <p class="text-muted">{!! str_limit( $project->description, 256)  !!}
            </p>
            <div class="pro-deadline m-b-15">
                <div class="sub-title">
                    Fecha de Entrega:
                </div>
                <div class="text-muted">
                    {{ $project->endDate }}
                </div>
            </div>
            <div class="project-members m-b-15">
                <div>Lider del Proyecto:</div>
                <ul class="team-members">
                    <li>
                        <a href="#" data-toggle="tooltip" title="{{$project->lider->username}}"><img src="{{asset('/images/users/'.$project->lider->photo)}}" alt="{{ $project->lider_id }}"></a>
                    </li>
                </ul>
            </div>
            <div class="project-members m-b-15">
                <div>Equipo :</div>
                <ul class="team-members">
                    @foreach ($project->users as $user)
                        <li>
                            <a href="#" data-toggle="tooltip" title="{{ $user->username}}">
                                <img src="{{asset('/images/users/'.$user->photo)}}" alt="{{$user->username}}">
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <p class="m-b-5">Progreso <span class="text-success pull-right">{{count($project->tasks) > 0 ? $project->progress() : 0}}%</span></p>
            <div class="progress progress-xs m-b-0">
                <div class="progress-bar progress-bar-success" role="progressbar" data-toggle="tooltip" title="40%" style="width: {{count($project->tasks) > 0 ? $project->progress() : 0}}%"></div>
            </div>
        </div>
    </div>
@endforeach

@include('projects.create')
@endsection

@section('scripts')
    <script type="text/javascript" src="/js/moment.min.js"></script>
    <script type="text/javascript" src="/js/select2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="plugins/summernote/dist/summernote.min.js"></script>
    <script>
            $(document).ready(function(){
                $('.summernote').summernote({
                    height: 200,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                 // set focus to editable area after initializing summernote
                });
                $('#team').change(function(){
                    var token = $('#csrf-token-id').attr('content');
                    var team = $(this).val();
                    $.ajax({
                        url: '/getUserInfo',
                        headers: {'X-CSRF-TOKEN': token},
                        type: 'POST',
                        dataType: 'json',
                        async: true,
                        data: { 
                            'team': team,
                        },
                        success: function(response){
                            if(response.status == true){
                                $('#teamContent').empty();
                                $.each(response.users, function(index,user){
                                    $('#teamContent').append(
                                        '<a href="/profile/'+user.id+'" data-toggle="tooltip" title="'+user.username+'">'+
                                        '<img src="/images/users/'+user.photo+'" class="avatar" alt="'+user.username+'" height="20" width="20">'+
                                        '</a>'
                                    )
                                });
                            }
                        }
                    });
                });

            });
    </script>
    <script>
        $( document ).ready(function() {
            $('.js-example-basic-multiple').select2();
            $('.datetimepicker').datetimepicker({
                format: 'YYYY/MM/DD'
            });
            $(".input-form").focus(function(){
                $(this).parent().addClass("focusWithText");
            });

            $( ".input-form" ).blur(function() {
                if($(this).val() == "")
                    $(this).parent().removeClass("focusWithText");
            });
        });
    </script>
    <!-- jQuery file upload -->
    <script src="../plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>

    <script src="../plugins/bower_components/moment/moment.js"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="../plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="../plugins/bower_components/jquery-asColor/dist/jquery-asColor.js"></script>
    <script src="../plugins/bower_components/jquery-asGradient/dist/jquery-asGradient.js"></script>
    <script src="../plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="../plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="../plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').change(function() {
        console.log(this.value);
    });
    $('#check-minutes').click(function(e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show').clockpicker('toggleView', 'minutes');
    });
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }
    // Colorpicker
    $(".colorpicker").asColorPicker();
    $(".complex-colorpicker").asColorPicker({
        mode: 'complex'
    });
    $(".gradient-colorpicker").asColorPicker({
        mode: 'gradient'
    });
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
    jQuery('#date-range').datepicker({
        toggleActive: true
    });
    jQuery('#datepicker-inline').datepicker({
        todayHighlight: true
    });
    // Daterange picker
    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        minDate: '06/01/2015',
        maxDate: '06/30/2015',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse',
        dateLimit: {
            days: 6
        }
    });
    </script>
@endsection