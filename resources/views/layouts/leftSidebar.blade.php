<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3>
                <span class="fa-fw open-close">
                    <img style="width: 42px;" src="{{ asset('images/haka0.png') }}">
                </span> 
                <span class="hide-menu">
                    <img  style="width: 115px;margin-left: 35px;" src="{{ asset('images/haka1.png') }}">
                </span>
            </h3> 
        </div>
        <ul class="nav" id="side-menu">
            {{--  <li><a href="{{ route( 'home' ) }}" class="waves-effect"><i class="icon-dashboard"></i><span class="hide-menu fix-menu">Dashboard</span></a> </li>  --}}
            <li><a href="{{ route('projects.index') }}" class="waves-effect"><i class="icon-project"></i><span class="hide-menu fix-menu">Proyectos</span></a></li>
            <li><a href="{{ route('tasks.index') }}" class="waves-effect"><i data-icon="7" class="icon-task"></i><span class="hide-menu fix-menu">Tareas</span></a></li>
            {{--  <li> <a href="javascript:void(0)" class="waves-effect"><i data-icon="/" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu fix-menu">tets<span class="fa arrow"></span><span class="label label-rouded label-purple pull-right">2</span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="javascript:void(0)"><i data-icon=")" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Second Level Item</span></a></li>
                    <li><a href="javascript:void(0)"><i class="fa-fw">S</i><span class="hide-menu"> Second Level Item</span></a></li>
                </ul>
            </li>  --}}
            {{--  <li> <a href="javascript:void(0)" class="waves-effect"><i data-icon="" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Multi Dropdown<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="javascript:void(0)"><i data-icon="/" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Second Level Item</span></a> </li>
                    <li> <a href="javascript:void(0)"><i data-icon="7" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Second Level Item</span></a> </li>
                    <li> <a href="javascript:void(0)" class="waves-effect"><i data-icon="&#xe008;" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Third Level </span><span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li> <a href="javascript:void(0)"><i class=" fa-fw">T</i><span class="hide-menu">Third Level Item</span></a> </li>
                            <li> <a href="javascript:void(0)"><i class=" fa-fw">M</i><span class="hide-menu">Third Level Item</span></a> </li>
                            <li> <a href="javascript:void(0)"><i class=" fa-fw">R</i><span class="hide-menu">Third Level Item</span></a> </li>
                            <li> <a href="javascript:void(0)"><i class=" fa-fw">G</i><span class="hide-menu">Third Level Item</span></a> </li>
                        </ul>
                    </li>
                </ul>
            </li>  --}}
        </ul>
    </div>
</div>
<!-- Left navbar-header end -->