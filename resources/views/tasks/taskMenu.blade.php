<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3>
                <span class="fa-fw open-close">
                    <img style="width: 42px;" src="{{ asset('images/haka0.png') }}">
                </span> 
                <span class="hide-menu">
                    <img  style="width: 115px;margin-left: 35px;" src="{{ asset('images/haka1.png') }}">
                </span>
            </h3> 
        </div>
        <ul class="nav" id="side-menu">
            <li><a href="{{ route('home') }}"><i class="icon-dashboard"></i> <span class="hide-menu fix-menu">Regresar Dashboard</span></a></li>
            <li><a href="{{ route('tasks.index') }}" class="waves-effect"><i class="icon-task"></i><span class="hide-menu fix-menu">Tareas</span></a></li>
            <li >
                <a href="javascript:void(0)" class="waves-effect">
                    {{--  <i class="icon-project"></i>  --}}
                    <span class="menu-title hide-menu">Mis Proyectos
                        <span style="margin-top: 10px;" class="fa fa-plus pull-right" data-toggle="modal" data-target="#create_project"></span>
                    </span>
                </a>
                
            </li>
            @foreach ($projects as $key => $project)
                <li><a href="/task-project/{{$project->slug}}" class="waves-effect"><i class="icon-project" style="background: url(../../images/projects/{{$project->photo}}); background-repeat: no-repeat; background-size: 26px 26px;"></i></i><span class="hide-menu fix-menu">{{$project->name}}</span></a></li>
            @endforeach
            
        </ul>
    </div>
</div>
<!-- Left navbar-header end -->