@extends('layouts.admin')

@section('title', 'Proyectos')

@section('css')
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="{{ asset('plugins/summernote/dist/summernote.css') }}" rel="stylesheet">

@endsection
@section('content')
<!-- .row -->
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <div class="col-xs-8">
                <h3 class="box-title">Tareas del proyecto: {{ $project->name }}</h3>
            </div>
            <div class="col-xs-4 text-right m-b-30">
                <ul class="nav navbar-nav pull-right chat-menu">
                    <li class="dropdown">
                        <a href="projects.html" class="grid-view btn btn-link active" data-toggle="dropdown"><i class="fa fa-bars"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)">Tareas Pendientes</a></li>
                            <li><a href="javascript:void(0)">Tareas Completadas</a></li>
                            <li><a href="javascript:void(0)">Todas</a></li>
                        </ul>
                    </li>
                </ul>
                <a  href="#" class="btn btn-primary rounded pull-right" data-toggle="modal" data-target="#create_task">
                    <i class="fa fa-plus"></i>
                        Nueva Tarea
                </a>
                
            </div>
            <div class="notification-popup hide">
                <p>
                    <span class="task"></span>
                    <span class="notification-text"></span>
                </p>
            </div>
            <div class="table-responsive manage-table task-wrapper">
                <table class="table " cellspacing="14" id="task-list">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th  width="650">NOMBRE</th>
                            <th class="text-center">PROGRESO</th>
                            <th class="text-center">PRIORIDAD</th>
                            <th class="text-center">USUARIO</th>
                            <th class="text-center">FECHA FINAL</th>
                            <th class="text-center">ACCIONES</th> 
                        </tr>
                    </thead>
                    <tbody  >
                        @foreach ($tasks as $task)
                            <tr  class="advance-table-row task-container task {!! $task->completed == '1'? 'completed active': '' !!}" id="{{ $task->id }}">
                                <td width="10"></td>
                                <td width="40">
                                    <span id="{{$task->completed}}" class="task-action-btn task-check">
                                        <span class="action-circle large complete-btn" title="Marcar Completa">
                                            <i class="material-icons">check</i>
                                        </span>
                                    </span>
                                </td>
                                <td width="720">
                                    <span class="task-label" contenteditable="false">{{ str_limit( $task->name, 72) }}</span>
                                </td>
                                <td width="320">
                                    <div class="bar-container">
                                        <div class="radio-task">
                                            <input type="radio" class="radio" name="progress" value="five" id="five">
                                            <label style="background:rgba(0, 0, 0, 0.25);" id="5" for="five" class="label">5%</label>
                                            
                                            <input type="radio" class="radio" name="progress" value="twentyfive" id="twentyfive">
                                            <label style="background:rgba(0, 0, 0, 0.25);" id="25" for="twentyfive" class="label">25%</label>
                                            
                                            <input type="radio" class="radio" name="progress" value="fifty" id="fifty">
                                            <label style="background:rgba(0, 0, 0, 0.25);" id="50" for="fifty" class="label">50%</label>
                                            
                                            <input type="radio" class="radio" name="progress" value="seventyfive" id="seventyfive">
                                            <label style="background:rgba(0, 0, 0, 0.25);" id="75" for="seventyfive" class="label">75%</label>
                                            
                                            <input type="radio" class="radio" name="progress" value="onehundred" id="onehundred">
                                            <label style="background:rgba(0, 0, 0, 0.25);" id="100" for="onehundred" class="label">100%</label>
                                        </div>
                                        
                                        <div class="progress">
                                            @if (empty($task->porcentage))
                                                <div class="progress-bar"></div>
                                            @else
                                                <div id="{{$task->porcentage}}" class="progress-bar  progress-{{$task->porcentage}}"></div>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                                {{--  <td>
                                    <div class="btn-group dropdown m-r-10">
                                        <button aria-expanded="false" data-toggle="dropdown" class="rounded btn btn-{!! str_replace(' ','-',$task->status) !!} dropdown-toggle waves-effect waves-light" type="button">
                                            {{ $task->status }}
                                            <span class="caret"></span>
                                        </button>
                                        <ul role="menu" class="dropdown-menu animated flipInY">
                                            <li><a href="javascript:void(0);">Action</a></li>
                                            <li><a href="javascript:void(0);">Another action</a></li>
                                            <li><a href="javascript:void(0);">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0);">Separated link</a></li>
                                        </ul>
                                    </div>
                                </td>  --}}

                                {{--  <td>
                                    <div class="btn-group m-r-10">
                                        <button aria-expanded="false" data-toggle="dropdown" class="rounded btn btn-info dropdown-toggle waves-effect waves-light" type="button"><span class="caret"></span></button>
                                        <ul role="menu" class="dropdown-menu animated flipInX">
                                            <li><a href="javascript:void(0);">Baja</a></li>
                                            <li><a href="javascript:void(0);">Media</a></li>
                                            <li><a href="javascript:void(0);">Alta</a></li>
                                        </ul>
                                    </div>
                                </td>  --}}

                                <td>
                                    <div class="dropdown action-label 
                                        @switch($task->priority)
                                            @case('Alta')
                                                dropdown-danger
                                            @break
                                            @case('Media')
                                                dropdown-warning
                                            @break
                                            @case('Normal')
                                                dropdown-info
                                            @break
                                            @case('Baja')
                                                dropdown-low
                                            @break
                                            @default
                                        @endswitch"
                                    >
                                        <a class="btn btn-white btn-sm rounded dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            @switch($task->priority)
                                                @case('Alta')
                                                    <i class="far fa-dot-circle text-danger"></i>
                                                @break
                                                @case('Media')
                                                    <i class="far fa-dot-circle text-primary"></i>
                                                @break
                                                @case('Normal')
                                                    <i class="far fa-dot-circle text-info"></i>
                                                @break
                                                @case('Baja')
                                                    <i class="far fa-dot-circle text-warning"></i>
                                                @break
                                                @default
                                            @endswitch
                                            {{$task->priority}}<i class="caret"></i>
                                        </a>
                                        <ul class="dropdown-menu  animated flipInX">
                                            <li><a href="#"><i class="far fa-dot-circle text-danger"></i> Alta</a></li>
                                            <li><a href="#"><i class="far fa-dot-circle text-primary"></i> Media</a></li>
                                            <li><a href="#"><i class="far fa-dot-circle text-info"></i> Normal</a></li>
                                            <li><a href="#"><i class="far fa-dot-circle text-warning"></i> Baja</a></li>
                                        </ul>
                                    </div>
                                </td>
                              
                                <td width="50" >
                                    <img class="center img-user-task" title="{{ $task->user->username }}" src="{{ asset('images/users/'. $task->user->photo)}}" class="img-circle cursor-pointer" width="30" /></td>
                                <td class="center">
                                    {{\Carbon\Carbon::parse($task->endDate)->format('d/m/Y')}}
                                    
                                </td>
                                <td width="100">
                                    <span class="task-action-btn task-btn-right center">
                                        <span class="action-circle large details-btn" title="Detalles">
                                            <i class="material-icons">toc</i>
                                        </span>
                                        <span class="action-circle large" title="Editar">
                                            <i class="material-icons">edit</i>
                                        </span>
                                        <span class="action-circle large delete-btn" title="Borrar tarea">
                                            <i class="material-icons">delete</i>
                                        </span>
                                    </span>
                                </td>
                               
                            </tr>
                            <tr id="separator-{{ $task->id }}">
                                <td colspan="7" class="sm-pd"></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('projects.create')
@include('tasks.create')
@include('tasks.details')

@endsection




@section('scripts')
    <script type="text/javascript" src="{{ asset('/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/summernote/dist/summernote.min.js') }}"></script>
    <script>
            $(document).ready(function(){
                $('.datetimepicker').datetimepicker({
                    format: 'YYYY/MM/DD'
                });
                $('.summernote').summernote({
                    height: 200,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                 // set focus to editable area after initializing summernote
                });
            });
    </script>
    <script>
        $( document ).ready(function() {
            $(".input-form").focus(function(){
                $(this).parent().addClass("focusWithText");
            });

            $( ".input-form" ).blur(function() {
                if($(this).val() == "")
                    $(this).parent().removeClass("focusWithText");
            });

           

           
        });
    </script>
    <!-- jQuery file upload -->
    <script src="../plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>

    <script type="text/javascript" src="{{ asset('plugins/summernote/dist/summernote.min.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/moment/moment.js') }}"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="{{ asset('plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js') }}"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="{{ asset('plugins/bower_components/jquery-asColor/dist/jquery-asColor.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/jquery-asGradient/dist/jquery-asGradient.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js') }}"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').change(function() {
        console.log(this.value);
    });
    $('#check-minutes').click(function(e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show').clockpicker('toggleView', 'minutes');
    });
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }
    // Colorpicker
    $(".colorpicker").asColorPicker();
    $(".complex-colorpicker").asColorPicker({
        mode: 'complex'
    });
    $(".gradient-colorpicker").asColorPicker({
        mode: 'gradient'
    });
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
    jQuery('#date-range').datepicker({
        toggleActive: true
    });
    jQuery('#datepicker-inline').datepicker({
        todayHighlight: true
    });
    // Daterange picker
    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        minDate: '06/01/2015',
        maxDate: '06/30/2015',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse',
        dateLimit: {
            days: 6
        }
    });
    </script>
    <script type="tect/template" id="task-template">
        <tr class="advance-table-row task-container task id="">
            <td width="10"></td>
            <td width="40">
                <span id="" class="task-action-btn task-check">
                    <span class="action-circle large complete-btn" title="Marcar Completa">
                        <i class="material-icons">check</i>
                    </span>
                </span>
            </td>
            <td width="720">
                <span class="task-label" contenteditable="false"></span>
            </td>
            <td width="320">
                <div class="bar-container">
                    <div class="radio-task">
                        <input type="radio" class="radio" name="progress" value="five" id="five">
                        <label id="5" for="five" class="label">5%</label>
                        
                        <input type="radio" class="radio" name="progress" value="twentyfive" id="twentyfive">
                        <label id="25" for="twentyfive" class="label">25%</label>
                        
                        <input type="radio" class="radio" name="progress" value="fifty" id="fifty">
                        <label id="50" for="fifty" class="label">50%</label>
                        
                        <input type="radio" class="radio" name="progress" value="seventyfive" id="seventyfive">
                        <label id="75" for="seventyfive" class="label">75%</label>
                        
                        <input type="radio" class="radio" name="progress" value="onehundred" id="onehundred">
                        <label id="100" for="onehundred" class="label">100%</label>
                    </div>
                    
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                </div>
            </td>
            <td>
                <div class="btn-group m-r-10">
                    <button aria-expanded="false" data-toggle="dropdown" class="newPriority rounded btn btn-info dropdown-toggle waves-effect waves-light" type="button">Normal<span class="caret"></span></button>
                    <ul role="menu" class="dropdown-menu animated flipInX">
                        <li><a href="javascript:void(0);">Baja</a></li>
                        <li><a href="javascript:void(0);">Media</a></li>
                        <li><a href="javascript:void(0);">Alta</a></li>
                    </ul>
                </div>
            </td>
            <td width="60" ><img title="" src="" class="img-circle" width="30" /></td>
            <td class="endDate">14/09/2019</td>
            <td width="160">
                <span class="task-action-btn task-btn-right ">
                    <span class="action-circle large" title="Detalles">
                        <i class="material-icons">toc</i>
                    </span>
                    {{--  <span class="action-circle large" title="Detalles">
                        <i class="material-icons">edit</i>
                    </span>  --}}
                    <span class="action-circle large delete-btn" title="Borrar tarea">
                        <i class="material-icons">delete</i>
                    </span>
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="7" class="sm-pd"></td>
        </tr>
    </script>
    <script type="tect/template" id="comment-template">
        <div class="white-box commentContent" id="">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="comment focusWithText">
                            <div>
                                <img class="img-comment" title="JuanJO" src="" class="img-circle cursor-pointer" width="30">
                                <a href="#"><p class="user-comment"></p></a>
                                <span class="comment-action-btn">
                                    <span class="action-circle large" title="Detalles">
                                        <i class="material-icons">edit</i>
                                    </span>
                                    <span class="action-circle large delete-btn" title="Borrar tarea">
                                        <i class="material-icons">delete</i>
                                    </span>
                                </span>
                                <span class="see-content">0
                                    <i class="fas fa-eye"></i>
                                </span>
                            </div>
                            <p class="helper helper1"> Hace 4min</p>
                            <div class="text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>


    <script type="text/javascript" src="{{ asset('js/task.js') }}"></script>
@endsection

