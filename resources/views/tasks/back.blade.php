@extends('layouts.admin')

@section('title', 'Proyectos')

@section('css')
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

@endsection
@section('content')
        

<div class="chat-main-row">
        <div class="chat-main-wrapper">
        <div class="col-xs-7 message-view task-view">
                <div class="chat-window">
                    <div class="chat-header">
                        <div style="padding: 1%;" class="white-box">
                            <div class="navbar">
                                <div class="col-xs-8">
                                    <h4 class="page-title">Tareas del proyecto: {{ $project->name }}</h4>
                                  

                                </div>
                                <div class="col-xs-4 text-right m-b-30">
                                    <ul class="nav navbar-nav pull-right chat-menu">
                                        <li class="dropdown">
                                            <a href="projects.html" class="grid-view btn btn-link active" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0)">Pending Tasks</a></li>
                                                <li><a href="javascript:void(0)">Completed Tasks</a></li>
                                                <li><a href="javascript:void(0)">All Tasks</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <a href="#" class="btn btn-primary rounded pull-right" data-toggle="modal" data-target="#create_task">
                                        <i class="fa fa-plus"></i>
                                         Nueva Tarea
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="chat-contents">
                        <div class="chat-content-wrap">
                            <div class="chat-wrap-inner">
                                <div class="chat-box">
                                    <div style="    padding: 0px;" class="task-wrapper">
                                        <div class="task-list-container">
                                            <div class="task-list-body">
                                                
                                                <ul id="task-list">
                                                    @foreach ($tasks as $task)
                                                        <li class="task {!! $task->completed == '1'? 'completed': '' !!}" id="{{ $task->id }}">
                                                            <div class="task-container">
                                                                <div class="col-md-6" >
                                                                    <span id="{{ $task->completed }}" class="task-action-btn task-check">
                                                                        <span class="action-circle large complete-btn" title="Mark Complete">
                                                                            <i class="material-icons">check</i>
                                                                        </span>
                                                                    </span>
                                                                    <span class="task-label" contenteditable="false">{{ $task->name }}</span>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="bar-container">
                                                                        <div class="radio-task">
                                                                            <input type="radio" class="radio" name="progress" value="five" id="five">
                                                                            <label id="5" for="five" class="label">5%</label>
                                                                            
                                                                            <input type="radio" class="radio" name="progress" value="twentyfive" id="twentyfive">
                                                                            <label id="25" for="twentyfive" class="label">25%</label>
                                                                            
                                                                            <input type="radio" class="radio" name="progress" value="fifty" id="fifty">
                                                                            <label id="50" for="fifty" class="label">50%</label>
                                                                            
                                                                            <input type="radio" class="radio" name="progress" value="seventyfive" id="seventyfive">
                                                                            <label id="75" for="seventyfive" class="label">75%</label>
                                                                            
                                                                            <input type="radio" class="radio" name="progress" value="onehundred" id="onehundred">
                                                                            <label id="100" for="onehundred" class="label">100%</label>
                                                                        </div>
                                                                        
                                                                        <div class="progress">
                                                                            @if (empty($task->porcentage))
                                                                                <div class="progress-bar"></div>
                                                                            @else
                                                                                <div class="progress-bar  progress-{{$task->porcentage}}"></div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <span class="task-action-btn task-btn-right pull-right">
                                                                        <span class="action-circle large" title="details">
                                                                            <i class="material-icons">toc</i>
                                                                        </span>
                                                                        <span class="action-circle large" title="Assign">
                                                                            <i class="material-icons">person_add</i>
                                                                        </span>
                                                                        <span class="action-circle large delete-btn" title="Delete Task">
                                                                            <i class="material-icons">delete</i>
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            {{--  <div class="task-list-footer">
                                                <div class="new-task-wrapper">
                                                    <textarea  id="new-task" placeholder="Enter new task here. . ."></textarea>
                                                    <span class="error-message hidden">You need to enter a task first</span>
                                                    <span class="add-new-task-btn btn" id="add-task">Add Task</span>
                                                    <span class="cancel-btn btn" id="close-task-panel">Close</span>
                                                </div>
                                            </div>  --}}
                                        </div>
                                    </div>
                                    <div class="notification-popup hide">
                                        <p>
                                            <span class="task"></span>
                                            <span class="notification-text"></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection


@include('projects.create')
@include('tasks.create')

@section('scripts')
<script>
        $( document ).ready(function() {
            $(".input-form").focus(function(){
                $(this).parent().addClass("focusWithText");
            });

            $( ".input-form" ).blur(function() {
                if($(this).val() == "")
                    $(this).parent().removeClass("focusWithText");
            });

           

           
        });
    </script>
    <!-- jQuery file upload -->
    <script src="../plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>

    <script type="text/javascript" src="{{ asset('plugins/summernote/dist/summernote.min.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/moment/moment.js') }}"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="{{ asset('plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js') }}"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="{{ asset('plugins/bower_components/jquery-asColor/dist/jquery-asColor.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/jquery-asGradient/dist/jquery-asGradient.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js') }}"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{ asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').change(function() {
        console.log(this.value);
    });
    $('#check-minutes').click(function(e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show').clockpicker('toggleView', 'minutes');
    });
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }
    // Colorpicker
    $(".colorpicker").asColorPicker();
    $(".complex-colorpicker").asColorPicker({
        mode: 'complex'
    });
    $(".gradient-colorpicker").asColorPicker({
        mode: 'gradient'
    });
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
    jQuery('#date-range').datepicker({
        toggleActive: true
    });
    jQuery('#datepicker-inline').datepicker({
        todayHighlight: true
    });
    // Daterange picker
    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        minDate: '06/01/2015',
        maxDate: '06/30/2015',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse',
        dateLimit: {
            days: 6
        }
    });
    </script>
    <script type="tect/template" id="task-template">
        <li class="task">
                <div class="task-container">
                    <div class="col-md-6" >
                        <span class="task-action-btn task-check">
                            <span class="action-circle large complete-btn" title="Mark Complete">
                                <i class="material-icons">check</i>
                            </span>
                        </span>
                        <span class="task-label" contenteditable="false"></span>
                    </div>
                    <div class="col-md-4">
                        <div class="bar-container">
                            <div class="radio-task">
                                <input type="radio" class="radio" name="progress" value="five" id="five">
                                <label id="5" for="five" class="label">5%</label>
                                
                                <input type="radio" class="radio" name="progress" value="twentyfive" id="twentyfive">
                                <label id="25" for="twentyfive" class="label">25%</label>
                                
                                <input type="radio" class="radio" name="progress" value="fifty" id="fifty">
                                <label id="50" for="fifty" class="label">50%</label>
                                
                                <input type="radio" class="radio" name="progress" value="seventyfive" id="seventyfive">
                                <label id="75" for="seventyfive" class="label">75%</label>
                                
                                <input type="radio" class="radio" name="progress" value="onehundred" id="onehundred">
                                <label id="100" for="onehundred" class="label">100%</label>
                            </div>
                            
                            <div class="progress">
                                <div class="progress-bar"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <span class="task-action-btn task-btn-right pull-right">
                            <span class="action-circle large" title="details">
                                <i class="material-icons">toc</i>
                            </span>
                            <span class="action-circle large" title="Assign">
                                <i class="material-icons">person_add</i>
                            </span>
                            <span class="action-circle large delete-btn" title="Delete Task">
                                <i class="material-icons">delete</i>
                            </span>
                        </span>
                    </div>
                </div>
            </li>
    </script>
    <script type="text/javascript" src="{{ asset('js/task.js') }}"></script>
@endsection

