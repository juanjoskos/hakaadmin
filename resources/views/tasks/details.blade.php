<div id="taskDetails" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4  class="modal-title nameTaskDetail"></h4>
            </div>
            <div class="modal-body">
                <div id="taskContentDetail" class="form new-task-wrapper">
                    <input type="hidden" name="project_id" value="{{ $project->id }}">
                    <div class="white-box">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="detail focusWithText">
                                        <p class="helper helper1">Nombre de la tarea</p>
                                        <p class="text nameTaskDetail"></p>
                                    </div>    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="detail focusWithText">
                                        <p class="helper helper1">Inicio</p>
                                        <p id="startDateDetail" class="text"></p>
                                    </div>  
                                </div>
                                <div class="col-md-3">
                                    <div class="detail focusWithText">
                                        <p class="helper helper1">Final</p>
                                        <p id="endDateDetail" class="text"></p>
                                    </div>  
                                </div>
                                <div class="col-md-3">
                                    <div class="detail focusWithText">
                                        <p class="helper helper1">Asignado</p>
                                        <p id="assignedDetail" class="text"></p>
                                    </div>  
                                </div>
                                <div class="col-md-3">
                                    <div class="detail focusWithText">
                                        <p class="helper helper1">Prioridad <i class="far fa-dot-circle text-danger"></i></p>
                                        <p id="priorityDetail" class="text"> </p>
                                    </div>  
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="detail focusWithText">
                                        <p class="helper helper1">Descripción</p>
                                        <div id="descriptionDetail" class="text"></div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white-box">
                        <div class="panel-body">
                            <h5 class="panel-title m-b-20">Imagenes Subidas</h5>
                            <div class="row">
                                <div class="col-md-3 col-sm-6">
                                    <div class="thumbnail">
                                        <div class="thumb">
                                            <img src="{{asset('images/logo2.jpg')}}" class="img-responsive" alt="">
                                        </div>
                                        <div class="caption text-center">
                                                demo.png
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <div class="thumbnail">
                                        <div class="thumb">
                                            <img src="{{asset('images/logo2.jpg')}}" class="img-responsive" alt="">
                                        </div>
                                        <div class="caption text-center">
                                                demo.png
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <div class="thumbnail">
                                        <div class="thumb">
                                            <img src="{{asset('images/logo2.jpg')}}" class="img-responsive" alt="">
                                        </div>
                                        <div class="caption text-center">
                                                demo.png
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <div class="thumbnail">
                                        <div class="thumb">
                                            <img src="{{asset('images/logo2.jpg')}}" class="img-responsive" alt="">
                                        </div>
                                        <div class="caption text-center">
                                                demo.png
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white-box">
                        <div class="panel-body">
                            <h5 class="panel-title m-b-20">Comentarios</h5>
                        </div>
                    </div>
                        
                    {{--  <div class="white-box">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="comment focusWithText">
                                    <div>
                                        <img class="img-comment" title="JuanJO" src="http://hakaadmin.loc/images/users/1568443486s-l300.jpg" class="img-circle cursor-pointer" width="30">
                                        <a href="#"><p class="user-comment">JuanJO</p></a>
                                        <span class="comment-action-btn">
                                            <span class="action-circle large" title="Detalles">
                                                <i class="material-icons">edit</i>
                                            </span>
                                            <span class="action-circle large delete-btn" title="Borrar tarea">
                                                <i class="material-icons">delete</i>
                                            </span>
                                        </span>
                                        <span class="see-content">0
                                            <i class="fas fa-eye"></i>
                                        </span>
                                    </div>
                                    <p class="helper helper1"> Hace 4min</p>
                                    <div class="text">
                                        {!! $comment->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  --}}
                </div>
                <form id="formComment" action="{{ route('tasks.addComment') }}" method="post" enctype="multipart/form-data" >
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                    <input type="hidden" name="task_id" id="taskID">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="inputGroup inputGroup1">
                                        <label>Comentario</label>
                                        <textarea style="padding: 22px 1em 0px;" name="description" rows="4" cols="5" class="input-form summernote"></textarea>
                                        <span class="indicator"></span>
                                    </div>   
                                </div>
                            </div>
                        </div>
                        <div class="m-t-20 text-center">
                            <button type="submit" class="button-haka" >Agregar Comentario</button>
                        </div>
                    </div>
                </form>
                
                </div>
            </div>
        </div>
    </div>
</div>