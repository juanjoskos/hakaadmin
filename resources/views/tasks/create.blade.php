@section('css')
    <link href="{{ asset('plugins/bower_components/register-steps/steps.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
    <link href="../plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <link href="../plugins/bower_components/jquery-asColorPicker-master/dist/css/asColorPicker.css" rel="stylesheet">
    <link href="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="../plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="plugins/summernote/dist/summernote.css" rel="stylesheet">
@endsection

<div id="create_task" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h4 class="modal-title">Crear Tarea</h4>
            </div>
            <div class="modal-body">
                <div class="form new-task-wrapper">
                    <form id="formTask" action="{{ route('tasks.store') }}" method="post" enctype="multipart/form-data" >
                        @csrf
                        <input type="hidden" name="project_id" value="{{ $project->id }}">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="inputGroup inputGroup1">
                                        <label>Nombre</label>
                                        <input id="new-task" type="text" class="input-form" name="name" required>
                                        <span class="error-message hidden">Debes ingresar asignar un nombre a la tarea</span>
                                        <p class="helper helper1">Nombre de la tarea</p>
                                        <span class="indicator"></span>
                                    </div>    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="inputGroup inputGroup1">
                                        <label>Inicio</label>
                                        <input id="startDate" name="startDate" class="input-form datetimepicker" required>
                                        <p class="helper helper1">Fecha de Inicio</p>
                                        <span class="indicator"></span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="inputGroup inputGroup1">
                                        <label>Final</label>
                                        <input id="endDate" name="endDate" class="input-form datetimepicker" required>
                                        <p class="helper helper1">Fecha de Entrega</p>
                                        <span class="indicator"></span>
                                    </div>    
                                </div>
                                <div class="col-md-3">
                                    <div class="inputGroup inputGroup1">
                                        <label>Prioridad</label>
                                        <select id="priority" name="priority" class="select input-form">
                                            <option></option>
                                            <option>Baja</option>
                                            <option>Media</option>
                                            <option>Alta</option>
                                        </select>
                                        <p class="helper helper1">Prioridad</p>
                                        <span class="indicator"></span>
                                    </div>
                                </div> 
                                <div class="col-md-3">
                                    <div class="inputGroup inputGroup1">
                                        <label>Asignar</label>
                                        <select name="user_id" class="select input-form" required>
                                            <option></option>
                                            @foreach ($users as $user)
                                                <option value="{{ $user->id }}">{{$user->username}}</option>
                                            @endforeach
                                        </select>
                                        <p class="helper helper1">Asignar Usuario</p>
                                        <span class="indicator"></span>
                                    </div>   
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="inputGroup inputGroup1">
                                            <label>Descripción</label>
                                            <textarea id="description" style="padding: 22px 1em 0px;" name="description" rows="4" cols="5" class="input-form summernote"></textarea>
                                            <span class="indicator"></span>
                                        </div>   
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="white-box">
                            <div class="m-t-20 text-center">
                                    {{--  data-dismiss="modal"  --}}
                                <button type="submit" id="add-task" class="button-haka add-new-task-btn" >Crear Tarea</button>
                            </div>
                        </div>
                    </form>
                </div>
               
                </div>
            </div>
        </div>
    </div>
</div>
    
