@extends('layouts.app')
@section('css')
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
@endsection
@section('content')
<div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url(../../assets/images/big/auth-bg.jpg) no-repeat center center;">
    <div class="auth-box" style="    max-width: 800px;  margin:0;">
        <div>
            <!-- Form -->
            <div class="row">
                <div class="col-12">
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{--  <div class="account-logo">
                                <a href="/login">
                                    <img style="    width: 272px;" src="images/logo2.png" alt="Focus Technologies">
                                </a>
                            </div>  --}}
                            <div class="white-box">
                                <input required name="photo" type="file" id="input-file-now" class="dropify" /> 
                            </div>
                        @empty($errors)
                            <span class="help-block">
                                <strong>{{ $errors }}</strong>
                            </span>
                        @endempty
                        
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="inputGroup inputGroup1">
                                    <label>Nombre</label>
                                    <input type="text" id="name" class="input-form" name="name" maxlength="256">
                                    <p class="helper helper1">Nombre</p>
                                    <span class="indicator"></span>
                                </div>    
                            </div>
                            <div class="col-md-6">
                                <div class="inputGroup inputGroup1">
                                    <label>Apellidos</label>
                                    <input type="text" id="lastName" class="input-form" name="lastName" maxlength="256">
                                    <p class="helper helper1">Apellidos</p>
                                    <span class="indicator"></span>
                                </div>    
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="col-md-6">
                                <div class="inputGroup inputGroup1">
                                    <label>Usuario</label>
                                    <input type="text" id="username" class="input-form" name="username" maxlength="256">
                                    <p class="helper helper1">Usuario</p>
                                    <span class="indicator"></span>
                                </div>    
                            </div>
                            
                            <div class="col-md-6">
                                <div class="inputGroup inputGroup1">
                                    <label>Correo</label>
                                    <input type="email" id="email" class="input-form" name="email" maxlength="256">
                                    <p class="helper helper1">Correo Electronico</p>
                                    <span class="indicator"></span>
                                </div>    
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="inputGroup inputGroup1">
                                    <label>Contraseña</label>
                                    <input type="password" id="password" class="input-form" name="password" maxlength="256">
                                    <p class="helper helper1">Contraseña</p>
                                    <span class="indicator"></span>
                                </div>    
                            </div>
                            <div class="col-md-6">
                                <div class="inputGroup inputGroup1">
                                    <label>Confirmar Contraseña</label>
                                    <input type="password" id="password_confirmation" class="input-form" name="password_confirmation" maxlength="256">
                                    <p class="helper helper1">Confirmar Contraseña</p>
                                    <span class="indicator"></span>
                                </div>    
                            </div>
                        </div>
                        
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="inputGroup inputGroup1">
                                        <label>Telefono</label>
                                        <input type="text" id="phone" class="input-form" name="phone" maxlength="256">
                                        <p class="helper helper1">Telefono</p>
                                        <span class="indicator"></span>
                                    </div>    
                                </div>
                                <div class="col-md-6">
                                    <div class="inputGroup inputGroup1">
                                        <label>Dirección</label>
                                        <input type="text" id="address" class="input-form" name="address" maxlength="256">
                                        <p class="helper helper1">Dirección</p>
                                        <span class="indicator"></span>
                                    </div>    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="inputGroup inputGroup1">
                                        <label>Cumpleaños</label>
                                        <input id='datetimepicker4'   name="birthday" class="input-form datetimepicker" type="text">
            
                                        <p class="helper helper1">Cumpleaños</p>
                                        <span class="indicator"></span>
                                    </div>    
                                </div>
                                <div class="col-md-6">
                                    <div class="inputGroup inputGroup1">
                                        <label>Rol</label>
                                        <select name="role" class="select input-form">
                                            <option value=""></option>
                                            <option value="CEO">CEO</option>
                                            <option value="Cliente">Cliente</option>
                                            <option value="Diseñador">Diseñador</option> 
                                            <option value="Marketing">Marketing</option>
                                            <option value="Modelador 3D">Modelador 3D</option>
                                            <option value="Programador">Programador</option>
                                        </select>
                                        <p class="helper helper1">Rol</p>
                                        <span class="indicator"></span>
                                    </div>   
                                </div>    
                            </div>
                            
            
                        <div class="white-box ">
                            <div class="m-t-20 text-center">
                                <button class="button-haka">Registrarse</button>
                            </div>
                            <div class="row m-t-10">
                                <div class="text-center">
                                    <a href="{{route('login')}}">Iniciar Sesión</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="/js/moment.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $( document ).ready(function() {
                
            $('#datetimepicker4').datetimepicker({
                format: 'YYYY/MM/DD'
            });
            $(".input-form").focus(function(){
                $(this).parent().addClass("focusWithText");
            });

            $( ".input-form" ).blur(function() {
                if($(this).val() == "")
                    $(this).parent().removeClass("focusWithText");
            });
        });
    </script>
    <script src="../plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
                
            }
            console.log("asd");

        })
        $('.dropify-wrapper').css('border-radius','100%');
        $('.dropify-wrapper').css('width','200px');
        $('.dropify-wrapper').css('margin','auto');


        $('#input-file-now').change(function(){
            setTimeout(StylePhoto,100)
        });

        function StylePhoto(){
            $('#photo').css('width','100%');
            $('#photo').css('height','100%');
            $('#photo').css('border-radius','100%');
            
        }

        

    });
    </script>
@endsection