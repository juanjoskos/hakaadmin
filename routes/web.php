<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index' );

Route::get('/dashboard', 'HomeController@index' )->name('home');

Auth::routes();



Route::resource('/projects', 'ProjectController');

// Task
Route::resource('/tasks', 'TaskController');
Route::get('/task-project/{slug}', 'TaskController@ProjectTask')->name('showTasks');
Route::post('/getTask', 'TaskController@GetTask');
Route::post('/addCommentTask', 'TaskController@AddCommentTask')->name('tasks.addComment');
Route::post('/deleteComment', 'TaskController@DeleteComment');
Route::post('/updateTask', 'TaskController@UpdateTask');
Route::post('/deleteTask', 'TaskController@DeleteTask');
Route::post('/getUserInfo', 'UserController@GetInfo');






