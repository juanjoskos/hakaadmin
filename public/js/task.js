;(function($){
	$(document).ready(function(){

		var notificationTimeout;
		var token = $('#csrf-token-id').attr('content');
		var barContainer;
		var taskParent;
		var name;
		var idTask;
		var progressBar;
		var completed;
		var porcentage;
		var taskTemplate = $($('#task-template').html());
		var commentTemplate = $($('#comment-template').html());

		//Shows updated notification popup 
		var updateNotification = function(task, notificationText, newClass){
			var notificationPopup = $('.notification-popup ');
			notificationPopup.find('.task').text(task);
			notificationPopup.find('.notification-text').text(notificationText);
			notificationPopup.removeClass('hide success');
			// If a custom class is provided for the popup, add It
			if(newClass)
				notificationPopup.addClass(newClass);
			// If there is already a timeout running for hiding current popup, clear it.
			if(notificationTimeout)
				clearTimeout(notificationTimeout);
			// Init timeout for hiding popup after 3 seconds
			notificationTimeout = setTimeout(function(){
				notificationPopup.addClass('hide');
			}, 3000);
		}

		// Adds a new Task to the todo list 
		var addTask = function($task, $user){
			// Get the new task entered by user
			var newTask = $('#new-task').val();
			// If new task is blank show error message
			if(newTask == ''){
				$('#new-task').addClass('error');
				$('.new-task-wrapper .error-message').removeClass('hidden');
				updateNotification('Error al crear tarea, ','Debes Llenar todos los datos', 'error');

			}
			else{
				var todoListScrollHeight = $('.task-list-body').prop('scrollHeight');
				// Make a new task template
				var newTemplate = $(taskTemplate).clone();
				// update the task label in the new template
				newTemplate.find('.task-label').text(newTask);
				// Add new class to the template
				newTemplate.addClass('new');
				// Remove complete class in the new Template in case it is present
				newTemplate.removeClass('completed');
				//Append the new template to todo list
				$('#task-list').append(newTemplate);
				// Clear the text in textarea
				$('#new-task').val('');
				// As a new task is added, hide the mark all tasks as incomplete button & show the mark all finished button
				$('#mark-all-finished').removeClass('move-up');
				$('#mark-all-incomplete').addClass('move-down');
				// Show notification
				// Smoothly scroll the todo list to the end
				$('.task-list-body').animate({ scrollTop: todoListScrollHeight}, 1000);
				updateNotification('Se ha añadido a la lista de tareas: ',newTask);
				newTemplate.attr('id',$task.id);
				newTemplate.find('.newPriority').text($task.priority);
				newTemplate.find('.newPriority').append('<span class="caret"></span>');
				newTemplate.find('.img-circle').attr('src','/images/users/'+$user.photo);
				newTemplate.find('.endDate').text($task.endDate);
			}
		}

		// Closes the panel for entering new tasks & shows the button for opening the panel
		var closeNewTaskPanel = function(){
			$('.add-task-btn').toggleClass('visible');
			$('.new-task-wrapper').toggleClass('visible');
			if($('#new-task').hasClass('error')){
				$('#new-task').removeClass('error');
				$('.new-task-wrapper .error-message').addClass('hidden');
			}
		}

		// Initalizes HTML template for a given task 

		// Shows panel for entering new tasks
		$('.add-task-btn').click(function(){
			var newTaskWrapperOffset = $('.new-task-wrapper').offset().top;
			$(this).toggleClass('visible');
			$('.new-task-wrapper').toggleClass('visible');
			// Focus on the text area for typing in new task
			$('#new-task').focus();
			// Smoothly scroll to the text area to bring the text are in view
			$('body').animate({ scrollTop: newTaskWrapperOffset}, 1000);
		});

		// Deletes task on click of delete button
		$('#task-list').on('click', '.task-action-btn .delete-btn', function(){
			var task = $(this).closest('.task');
			var taskText = task.find('.task-label').text();
			task.remove();
			$('#separator-'+task.attr('id')).remove();
			GetTask($(this));

			$.ajax({
				url: '/deleteTask',
				headers: {'X-CSRF-TOKEN': token},
				type: 'POST',
				dataType: 'json',
				async: true,
				data: { 
					'task_id': idTask,
				},
				success: function(response){
					if(response['status'] == true){
						updateNotification('Se ha eliminado de la lista de tareas:', taskText);
						
					}
				}
			});
		});

		// Show task details  on click of details button
		$('#task-list').on('click', '.task-action-btn .details-btn', function(){
			var idTask = $(this).closest('.task').attr('id');

			$.ajax({
				url: '/getTask',
				headers: {'X-CSRF-TOKEN': token},
				type: 'POST',
				dataType: 'json',
				async: true,
				data: { 
					'task_id': idTask,
				},
				success: function(response){
					if(response.status == true){
						$('#taskID').val(response.task.id);
						$('.nameTaskDetail').text(response.task.name);
						$('#startDateDetail').text(response.task.startDate);
						$('#endDateDetail').text(response.task.endDate);
						$('#descriptionDetail').append(response.task.description);
						$('#priorityDetail').text(response.task.priority);
						$('#assignedDetail').text(response.task.user.username);
						$('#taskDetails').modal('show');
						$.each(response.task.comments, function(index,comment){
							var newTemplate = $(commentTemplate).clone();
							newTemplate.attr('id',comment.id);
							$('#taskContentDetail').append(newTemplate);
							newTemplate.find('.img-comment').attr('src','/images/users/'+comment.user.photo);
							newTemplate.find('.user-comment').text(comment.user.username);
							newTemplate.find('.text').append(comment.description);
						});
					
					}
				}
			});
		});

		// Marks a task as complete
		$('#task-list').on('click', '.task-action-btn .complete-btn', function(){
			var task = $(this).closest('.task');
			var taskText = task.find('.task-label').text();
			var newTitle = task.hasClass('completed') ? 'Mark Complete' : 'Mark Incomplete';
			$(this).attr('title', newTitle);
			task.hasClass('completed') ? updateNotification(taskText, 'marcado como Incompleto.') : updateNotification(taskText, ' marcado como Completo..', 'success');
			task.toggleClass('completed');
			task.toggleClass('active');

			GetTask($(this));

			if(completed.attr('id') == 0){
				completed.attr('id','1');
				porcentage = 100;

			}else{
				completed.attr('id','0');
				porcentage = 0;

			}
			UpdateProgress();
			UpdateTask();


		});

		// Adds a task on hitting Enter key, hides the panel for entering new task on hitting Esc. key
		// $('#new-task').keydown(function(event){
		// 	// Get the code of the key that is pressed
		// 	var keyCode = event.keyCode;
		// 	var enterKeyCode = 13;
		// 	var escapeKeyCode = 27;
		// 	// If error message is already displayed, hide it.
		// 	if($('#new-task').hasClass('error')){
		// 		$('#new-task').removeClass('error');
		// 		$('.new-task-wrapper .error-message').addClass('hidden');
		// 	}
		// 	// If key code is that of Enter Key then call addTask Function
		// 	if(keyCode == enterKeyCode){
		// 		event.preventDefault();
		// 		addTask();
		// 	}
		// 	// If key code is that of Esc Key then call closeNewTaskPanel Function
		// 	else if(keyCode == escapeKeyCode)
		// 		closeNewTaskPanel();

		// });

		// Add new task on click of add task button
		$('#formTask').submit(function(e) {
			$('.preloader').removeClass('d-none');
			
			e.preventDefault(); // avoid to execute the actual submit of the form.

			var form = $(this);
			var url = form.attr('action');

			$.ajax({
				type: "POST",
				headers: {'X-CSRF-TOKEN': token},
				url: url,
				data: new FormData(this),
				contentType:false,
				processData: false,
				success: function(response)
				{
					if(response.status == true){
						addTask(response.task, response.user);
						$('#formTask').trigger("reset");
						$('#create_task').modal('hide');
						$('#description').summernote('reset');
						
					}else{
						$('#loader').addClass('d-none');
						updateNotification('Error al crear tarea, ','Debes Llenar todos los datos', 'error');
					}
				}
			});
			
		});
		
		$('#formComment').submit(function(e) {
			$('.preloader').removeClass('d-none');
			
			e.preventDefault(); // avoid to execute the actual submit of the form.

			var form = $(this);
			var url = form.attr('action');

			$.ajax({
				type: "POST",
				headers: {'X-CSRF-TOKEN': token},
				url: url,
				data: new FormData(this),
				contentType:false,
				processData: false,
				success: function(response)
				{
					if(response.status == true){
						addTask(response.task, response.user);
						$('#formComment').trigger("reset");
						updateNotification('Comentario Agregado, ','Su comentario se ha agregado');
						var newTemplate = $(commentTemplate).clone();
						newTemplate.attr('id',response.comment.id);
						$('#taskContentDetail').append(newTemplate);
						newTemplate.find('.img-comment').attr('src','/images/users/'+response.user.photo);
						newTemplate.find('.user-comment').text(response.user.username);
						newTemplate.find('.text').append(response.comment.description);
						
					}else{
						$('#loader').addClass('d-none');
						updateNotification('Error al crear tarea, ','Debes Llenar todos los datos', 'error');
					}
				}
			});
			
		});


		$('#taskContentDetail').on('click', '.comment-action-btn .delete-btn', function(){
			var comment = $(this).closest('.commentContent');
			var commentText = comment.find('.text').text();
			var idComment = comment.attr('id');

			comment.remove();

			$.ajax({
				url: '/deleteComment',
				headers: {'X-CSRF-TOKEN': token},
				type: 'POST',
				dataType: 'json',
				async: true,
				data: { 
					'comment_id': idComment,
				},
				success: function(response){
					if(response['status'] == true){
						updateNotification('Se ha eliminado el comentario:', commentText);
						
					}
				}
			});
		});


		// Close new task panel on click of close panel button
		$('#close-task-panel').click(closeNewTaskPanel);

		// Mark all tasks as complete on click of mark all finished button
		$('#mark-all-finished').click(function(){
			$('#task-list .task').addClass('completed');
			$('#mark-all-incomplete').removeClass('move-down');
			$(this).addClass('move-up');
			updateNotification('All tasks', 'marcado como Completo..', 'success');
		});

		// Mark all tasks as incomplete on click of mark all incomplete button
		$('#mark-all-incomplete').click(function(){
			$('#task-list .task').removeClass('completed');
			$(this).addClass('move-down');
			$('#mark-all-finished').removeClass('move-up');
			updateNotification('All tasks', 'marcado como Incompleto..');
		});


		$('#task-list').on('click', '.progress', function(){
			GetTask($(this));
			barContainer.children('div.radio-task').css('display', 'block');
		});

		$('#task-list').on('click', '.label', function(){
			porcentage = $(this).attr('id');

			UpdateProgress();
			
			
			if(porcentage == 100){
				taskParent.addClass('completed');
				updateNotification('All tasks', 'marcado como Completo..');
				completed.attr('id','1');

			}else{
				taskParent.removeClass('completed');
				completed.attr('id','0');
			}
			
			$('.radio-task').css('display', 'none');

			
			UpdateTask();

			
		});

		function GetTask(obj){
			taskParent =obj.parents('.task');
			name = taskParent.find('.task-label').text();
			idTask = taskParent.attr('id');
			barContainer = taskParent.find('.bar-container');
			progressBar = barContainer.children('.progress').children('.progress-bar');
			completed = taskParent.find('.task-action-btn');
			porcentage = taskParent.find('.progress-bar').attr('id');
		}
		function UpdateTask(){
			$.ajax({
				url: '/updateTask',
				headers: {'X-CSRF-TOKEN': token},
				type: 'POST',
				dataType: 'json',
				async: true,
				data: { 
					'task_id': idTask,
					'porcentage': porcentage,
					'name': name,
					'completed': completed.attr('id')
				},
				success: function(response){
					if(response['status'] == true){
					}
				}
			});
		}

		function UpdateProgress(){
			progressBar.removeClass('progress-0');
			progressBar.removeClass('progress-5');
			progressBar.removeClass('progress-25');
			progressBar.removeClass('progress-50');
			progressBar.removeClass('progress-75');
			progressBar.removeClass('progress-100');

			
			progressBar.addClass('progress-'+porcentage);
			progressBar.attr('id',porcentage);
		}

		
		

	});
}(jQuery))