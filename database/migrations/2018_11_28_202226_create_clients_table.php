<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('name');
            $table->string('lastName');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->string('password');
            $table->string('phone');
            $table->string('address')->nullable();
            $table->string('company');
            $table->timestamps();
        });

        Schema::create('permissions_clients', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('permission_id');
            $table->unsignedInteger('client_id');
            $table->foreign('permission_id')->references('id')->
                            on('permissions')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->
                            on('clients')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
