<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('projects');
            $table->date('startDate');
            $table->date('endDate');
            $table->integer('porcentage')->default('0')->nullable();
            $table->enum('status',['Pendiente','En Proceso','Subida','En Revision','Terminada'])->default('Pendiente')->nullable();
            $table->enum('priority',['Alta','Media','Normal','Baja']);
            $table->boolean('completed')->default('0')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('comment_tasks', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('comment_id');
            $table->unsignedInteger('task_id');
            $table->foreign('comment_id')->references('id')->
                            on('comments')->onDelete('cascade');
            $table->foreign('task_id')->references('id')->
                            on('tasks')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('files_task', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('file_id');
            $table->unsignedInteger('task_id');
            $table->foreign('file_id')->references('id')->
                            on('files')->onDelete('cascade');
            $table->foreign('task_id')->references('id')->
                            on('tasks')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
