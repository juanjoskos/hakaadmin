<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('photo')->nullable();
            $table->string('name');
            $table->integer('client_id')->unsigned()->nullable();;
            $table->foreign('client_id')->references('id')->on('clients');
            $table->integer('leader_id')->unsigned();
            $table->foreign('leader_id')->references('id')->on('users'); 
            $table->date('startDate');
            $table->date('endDate');
            $table->enum('priority',['Alta','Media','Normal','Baja']);
            $table->text('description')->nullable();
            $table->string('slug')->unique();
            $table->timestamps();
        });

        Schema::create('users_projects', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('project_id');
            $table->foreign('user_id')->references('id')->
                            on('users')->onDelete('cascade');
            $table->foreign('project_id')->references('id')->
                            on('projects')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('files_projects', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('file_id');
            $table->unsignedInteger('project_id');
            $table->foreign('file_id')->references('id')->
                            on('files')->onDelete('cascade');
            $table->foreign('project_id')->references('id')->
                            on('projects')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
